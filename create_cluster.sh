#!/bin/bash

cluster_name="$1"

# Gather all arguments other than the cluster_name into the $args variable as an array
temp=("$@")
args=("${temp[@]:1}")

# Create the cluster
response=$(gcloud container clusters create "$cluster_name" --num-nodes 1 --format="value[separator=';'](masterAuth.clusterCaCertificate,zone,endpoint)" "${args[@]}") || exit 1

# Split the response into an array
IFS=';' read -ra cluster_details <<< "$response"

certificate=${cluster_details[0]}
zone=${cluster_details[1]}
endpoint=${cluster_details[2]}

gcloud container clusters get-credentials "$cluster_name" --zone "$zone" || exit 1

cat <<'EOF' | kubectl apply -f - || exit 1
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF

echo ''
kubectl -n kube-system describe secret "$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')" | grep 'token:'

echo ''
echo 'IP: ' "$endpoint"

# To get the IP using a standalone command:
# echo 'IP: ' $(gcloud container clusters describe $cluster_name --format="value(endpoint)")

echo ''
echo 'Cluster CA Certificate:'
echo "$certificate" | base64 --decode

# To get the certificate using a standalone command:
# gcloud container clusters describe $cluster_name --format="value(masterAuth.clusterCaCertificate)" | base64 -d
