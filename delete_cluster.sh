#!/bin/bash

cluster_name="$1"

# Gather all arguments other than the cluster_name into the $args variable as an array
temp=("$@")
args=("${temp[@]:1}")

gcloud container clusters get-credentials "$cluster_name" "${args[@]}" || exit 1

# Get names of physical volumes using kubectl.
response=$(kubectl get pv --no-headers=true | awk '{print $1}') || exit 1

# Create array by splitting the response by the newline char
IFS=$'\n' read -ra pv_names <<< "$response" || exit 1

# Get disk names from gcloud only if there are any physical volumes
if [ ${#pv_names[@]} -gt 0 ]; then
    # Create a string from the names array. Each element is separated by '|' char.
    # The '|' character is used because the "gcloud compute disks list" command
    # accepts a regex string for filtering.
    printf -v pv_names_str '|%s' "${pv_names[@]}" || exit 1
    # Remove the first '|' char.
    pv_names_str=${pv_names_str:1} || exit 1

    # Get disk names from gcloud.
    disk_names=$(gcloud compute disks list --filter="name~'$pv_names_str'" --format="value[terminator=','](name)" "${args[@]}") || exit 1
    # Split string by ',' and create an array.
    IFS=',' read -ra disk_names <<< "$disk_names"
fi

# Delete GKE cluster
gcloud container clusters delete "$cluster_name" "${args[@]}" || exit 1

# Delete any persistent volume disks that were created for the cluster.
if [ "${#disk_names[@]}" -gt 0 ]; then
    gcloud compute disks delete "${disk_names[@]}" "${args[@]}" || exit 1
fi
